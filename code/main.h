#ifndef MAIN_H
#define MAIN_H

///Shits
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>

/// Standard
#include <stm32f4xx.h>
#include "stm32f4xx_usart.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_spi.h"

/// TM_Libs
#include "tm_stm32f4_button.h"
#include "tm_stm32f4_adc.h"
#include "tm_stm32f4_gpio.h"
#include "tm_stm32f4_pwm.h"
#include "tm_stm32f4_i2c.h"
#include "tm_stm32f4_timer_properties.h"
#include "tm_stm32f4_delay.h"


/// Headers
#include "gyro.h"
#include "hardware.h"
#include "outskirts.h"
#include "usart.h"
#include "ir.h"
#include "motors.h"
#include "encoders.h"
#include "motion.h"
#include "display.h"
#include "actionq.h"
#include "config.h"
#include "algorithm.h"
#include "pq.h"
#include "messenger.h"

#endif
