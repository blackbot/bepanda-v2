#ifndef OUTSKIRTS_H
#define OUTSKIRTS_H

void Outskirts_Init(void);
void SetLed(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, uint8_t x);
void SetLedInt(uint8_t x);

void FunnyLeds(void);

void Buzzer(int16_t freq, int16_t duration);
void CheckBuzzerTime(void);

void BUTTON1_EventHandler(TM_BUTTON_PressType_t type);
void BUTTON2_EventHandler(TM_BUTTON_PressType_t type); 
void Buttons_task(void* pdata);
void BatteryLevel(void);
#endif

