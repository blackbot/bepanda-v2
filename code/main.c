/*---------------------------- Include ---------------------------------------*/
#include "main.h"

void SysTick_Handler()
{

	//CheckBuzzerTime();
	MeasureDistance();
	if(fUseController)
		ControlLoop();
	//GyroUpdate(); //uruchamiane co 2ms
}


int main(void)
{
	TM_DELAY_Init();
	SystemInit();
	SysTick_Config(SystemCoreClock/1000);
	SystemCoreClockUpdate();
	USART_Configuration();

	printf("\n");
	printf("--------------------------\n");
	printf("- BePanda v2 is booting! -\n");
	printf("---- by BlackBotTeam  ----\n");
	printf("--------------------------\n");

	Outskirts_Init();
	IR_Init();
	Motors_Init();
	Encoders_Init();
	Display_Init();
	Gyro_Init();
	Maze_Init();
	//PrintfMaze();
	Config();
	FunnyLeds();

	while (distanceLC > 50){
		DisplayData("W8=D");
		Delayms(500);
	}

	for (int i = 0; i < 2; ++i)
	{
		SetLed(LED_LS, ON);
		Delayms(200);
		SetLed(LED_LS, OFF);
		Delayms(200);
	}

	Delayms(200);
	fUseController = ON;
	fAlgorithm = ON;

	ActionQInsert(Move, 1);

	while (1)
	{
		//printf("pos:(%d,%d) %d\r\n", posX, posY, orientation);
		TM_BUTTON_Update();
		BatteryLevel();
		if (fAlgorithm == ON)
		{
			fAlgorithm = 0;
			ComputeAlgorithm();
		}

		//		printf("POS=%d\r\n", tempPos);
		//		for (int i = 0; i < 7; ++i)
		//		{
		//			for (int j = 0; j < 7; ++j)
		//				printf("%d\t", wallAppearance[i][j]);
		//			printf("\r\n");
		//		}
		//		printf("\r\n");
		Delayms(100);
	}
}

