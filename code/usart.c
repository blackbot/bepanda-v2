
#include "main.h"
#include "misc.h"

extern void PrintChar(char c);

#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

void USART_NVIC_Config(void);

/*******************************************************************************
* Function Name  : USART_Configuration
* Description    : Configure USART
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void USART_Configuration(void)
{ 												
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	// auto variables have undefined values so be safe
	GPIO_StructInit (&GPIO_InitStructure);
	USART_StructInit (&USART_InitStructure);
	// Make sure the USART and the GPIO port get a clock
	USART_CLK_CMD (USART_CLK, ENABLE);
	USART_GPIO_CLK_CMD (USART_GPIO_CLK, ENABLE);
	// configure the RX and TX pin as AF or nothing will appear
	GPIO_InitStructure.GPIO_Pin = USART_RX_PIN | USART_TX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
	GPIO_Init (USART_GPIO, &GPIO_InitStructure);
	// Connect USART pins to AF or there was no point in setting them up
	GPIO_PinAFConfig (USART_GPIO, USART_TX_AF_SRC, GPIO_AF_USART);
	GPIO_PinAFConfig (USART_GPIO, USART_RX_AF_SRC, GPIO_AF_USART);
	//Other than the speed, all these are the default values
	USART_InitStructure.USART_BaudRate = USART_BAUD;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init (USART, &USART_InitStructure);

	/* Enable the USART Transmit interrupt: this interrupt is generated when the
	USART transmit data register is empty */
	USART_ITConfig(USART,USART_IT_RXNE,ENABLE);

	USART_Cmd (USART, ENABLE);
	USART_NVIC_Config();

}

void USART_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable the USARTx Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void USARTx_IRQHANDLER(void)
{
	char ch;
	if(USART_GetITStatus(USART, USART_IT_RXNE) != RESET)
	{
	ch = USART_ReceiveData(USART);       // get received character
	if (ch=='\r') ch='\n';
	PrintChar(ch);
	}
}
/* Use no semihosting */
#if 0
#pragma import(__use_no_semihosting)
struct __FILE
{  
	int handle;
};
FILE __stdout;

int _sys_exit(int x)
{
	x = x;
	return(0);
}
#endif

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART */
	USART_SendData(USART, (uint8_t) ch);

	/* Loop until the end of transmission */
	while (USART_GetFlagStatus(USART, USART_FLAG_TC) == RESET)
	{}

	return ch;
}
