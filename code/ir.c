#include "main.h"
#include <stdio.h>
#include <math.h>

double distanceRC;
double distanceRS;
double distanceRD;
double distanceLD;
double distanceLS;
double distanceLC;

int16_t iterationDiode = 0;

//tabela wspolczynnikow funkcji logistic:
//              (A1 - A2)
// y = A2 + -----------------
//          1 + (x / x0) ^ p

				    //RC	   //LC		  //RS		  //LS		   //RD		  //LD
double A1[6] = {12261.376,	5490.311, 	815.884,	492.749,	2231.150,	539.440};
double A2[6] = {    3.049,	   0.974,	 -1.927,	 -3.154,	  -0.248,	 -2.390};
double x0[6] = {    1.272,	   0.216,     0.256,	  0.361,	   0.354,	  0.348};
double  p[6] = {    1.375,	   0.791,     0.543,	  0.486,	   0.806,     0.552};


void IR_Init(void)
{
	TM_ADC_Init(TT_RC);
	TM_ADC_Init(TT_RD);
	TM_ADC_Init(TT_RS);
	TM_ADC_Init(TT_LS);
	TM_ADC_Init(TT_LD);
	TM_ADC_Init(TT_LC);

	TM_GPIO_Init(IR_RC, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(IR_RD, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(IR_RS, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(IR_LS, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(IR_LD, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(IR_LC, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);

}


double CalculateWithoutGain(int8_t i, double x)
{
	return (A2[i] + ((A1[i] - A2[i]) / (1 + pow((x / x0[i]), p[i])) )) * 10; //[mm]
	//return x;
}

int16_t CalculateWithGain(int16_t x)
{
	return x;
}

void IR_process(ADC_TypeDef* ADCx, uint8_t channel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, int16_t gainMode, double *result)
{
	static int16_t withIR = 0;
	static int16_t withoutIR = 0;
	static int16_t modeMeasure = 0;

	switch(modeMeasure)
	{
		case 0:
			withoutIR = TM_ADC_Read(ADCx, channel);
			modeMeasure++;
			break;
		case 1:
			TM_GPIO_SetPinHigh(GPIOx, GPIO_Pin);
			modeMeasure++;
			break;
		case 2:
			withIR = TM_ADC_Read(ADCx, channel);
			TM_GPIO_SetPinLow(GPIOx, GPIO_Pin);
			///TODO: czy ten warunek na pewno nie dziala??
			if(*result < WALL_AVAILABLE || !gainMode)
			{
				*result = CalculateWithoutGain(iterationDiode, withIR - withoutIR);
				modeMeasure = 1;
				iterationDiode++;
			}
			else
				modeMeasure++;
			break;
		case 3:
			//TM_GPIO_SetPinHigh(IR_GAIN);
			TM_GPIO_SetPinHigh(GPIOx, GPIO_Pin);
			modeMeasure++;
			break;
		case 4:
			withIR = TM_ADC_Read(ADCx, channel);
			modeMeasure++;
			break;
		case 5:
			*result = CalculateWithoutGain(iterationDiode, withIR - withoutIR);
			modeMeasure = 0;
			iterationDiode++;
			break;
	}
}

void MeasureDistance()
{
	if(iterationDiode > 5) iterationDiode = 0;

	switch(iterationDiode)
	{
		case 0:
			IR_process(TT_RC, IR_RC, OFF, &distanceRC);
			break;
		case 1:
			IR_process(TT_LC, IR_LC, OFF, &distanceLC);
			break;
		case 2:
			IR_process(TT_RS, IR_RS, OFF, &distanceRS);
			break;
		case 3:
			IR_process(TT_LS, IR_LS, OFF, &distanceLS);
			break;
		case 4:
			IR_process(TT_RD, IR_RD, OFF, &distanceRD);
			break;
		case 5:
			IR_process(TT_LD, IR_LD, OFF, &distanceLD);
			break;
	}
}
