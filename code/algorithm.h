#ifndef ALGORITHM_H
#define ALGORITHM_H

#include "enums.h"

typedef struct
{
	int8_t ST, ND;
}PII;

PII PII_(int8_t, int8_t);

void Maze_Init(void);
int8_t ComputeAlgorithm(void);
void FloodFill (int8_t startX, int8_t startY, int8_t endX, int8_t endY, ModeNum mode);
void DetermineMoves (int8_t coordX, int8_t coordY, int8_t *orient, ModeNum mode);

extern int8_t posX, posY;
extern int8_t endX, endY;
extern OrientationNum orientation;

extern int16_t maze[MAZE_SIZE*2 + 2][MAZE_SIZE*2 + 2];
extern int8_t avMoves[8];
extern int16_t mazeSizeCal;

#endif //ALGORITHM_H
