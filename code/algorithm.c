#include "main.h"

//TODO: jesli chcemy skosy to rozmiar do 16
int8_t avMoves[8] = {0, 1, 1, 0, 0, -1, -1, 0};

int8_t posX, posY;
int8_t endX, endY;
OrientationNum orientation;

int16_t maze[MAZE_SIZE*2 + 2][MAZE_SIZE*2 + 2];
int16_t mazeSizeCal = MAZE_SIZE*2 + 1;

int16_t time[MAZE_SIZE*2 + 2][MAZE_SIZE*2 + 2];
int16_t timeBest[MAZE_SIZE*2 + 2][MAZE_SIZE*2 + 2];
PII prev[MAZE_SIZE*2 + 2][MAZE_SIZE*2 + 2];

PII pointBest[MAZE_SIZE*MAZE_SIZE];
int16_t pointBestCount;
Heap h;

PII PII_(int8_t x, int8_t y)
{
	PII p;
	p.ST = x;
	p.ND = y;
	return p;
}

double Dot(int x1, int y1, int x2, int y2)
{
	return x1 * x2 + y1 * y2;
}

double Det(int x1, int y1, int x2, int y2)
{
	return x1 * y2 - x2 * y1;
}

void Maze_Init()
{
	for (int i = 0; i < mazeSizeCal; ++i)
	{
		for (int j = 0; j < mazeSizeCal; ++j)
		{
			if (!((i&1) || (j&1))) //slupki
				maze[i][j] = WALLCHECKED;
			if (i == INIT_ENDX && j == INIT_ENDY)
				maze[i][j] &= ~WALL;
		}
	}
	for (int i = 0; i < mazeSizeCal; ++i)
	{
		maze[0][i] = maze[i][0] = WALLCHECKED;
		maze[mazeSizeCal-1][i] = maze[i][mazeSizeCal-1] = WALLCHECKED;
	}

	maze[INIT_STARTX - 1][INIT_STARTY] = WALLCHECKED;
	maze[INIT_STARTX][INIT_STARTY] = CHECKED;
	maze[INIT_STARTX + 1][INIT_STARTY] = WALLCHECKED;

	posX = INIT_STARTX;
	posY = INIT_STARTY;
	orientation = N;
}

int16_t AddWeight (PII from, PII acc, PII to)
{
	if (from.ST == acc.ST && from.ND == acc.ND)
	{
		switch (orientation)
		{
		case N:
			from = PII_(acc.ST, acc.ND - 1);
			break;
		case E:
			from = PII_(acc.ST - 1, acc.ND);
			break;
		case S:
			from = PII_(acc.ST, acc.ND + 1);
			break;
		case W:
			from = PII_(acc.ST + 1, acc.ND);
			break;
		}
	}

	double vectorProduct = Det(to.ST - acc.ST, to.ND - acc.ND, acc.ST - from.ST, acc.ND - from.ND);
	double scalarProduct = Dot(to.ST - acc.ST, to.ND - acc.ND, acc.ST - from.ST, acc.ND - from.ND);

	if (vectorProduct == 0) //rownolegle
	{
		if (scalarProduct < 0) //180
			return BACK_WEIGHT;
		else
			return STRAIGHT_WEIGHT;
	}
	if (scalarProduct == 0)
		return TURN_WEIGHT;

	return INF;
}

int8_t ComputeAlgorithm()
{
	PII minField;
	int32_t minWeight = INF;
	pointBestCount = 0;
	
	printf("ALG 1\r\n");
	FloodFill(INIT_STARTX, INIT_STARTY, INIT_ENDX, INIT_ENDY, BEST);
	printf("ALG 2\r\n");
	FloodFill(posX, posY, INIT_ENDX, INIT_ENDY, MY_POS);
	
	if(pointBestCount == 0) return 1;
	
	for(int i=0; i<pointBestCount; i++)
	{
		if(time[pointBest[i].ST][pointBest[i].ND] < minWeight)
		{
			minField.ST = pointBest[i].ST;
			minField.ND = pointBest[i].ND;
			minWeight = time[pointBest[i].ST][pointBest[i].ND];
		}
	}

//	DisplayData("4 FF");
	printf("ALG 3\r\n");
	FloodFill(posX, posY, minField.ST, minField.ND, GOAL);
	
	return 0;
}

void FloodFill (int8_t startX, int8_t startY, int8_t endX, int8_t endY, ModeNum mode)
{
	printf("(%d,%d)->(%d,%d)\r\n", startX, startY, endX, endY);
	int8_t x = startX;
	int8_t y = startY;
	int8_t tempOrient = (mode == BEST)? N : orientation;

	for (int i = 0; i < mazeSizeCal; ++i)
	{
		for (int j =0; j < mazeSizeCal; ++j)
		{
			time[i][j] = INF;
			prev[i][j] = PII_(-1,-1);
		}
	}

	time[x][y] = 0;
	prev[x][y] = PII_(x,y);

	h.size = 0;

	Vertex temp = Vertex_(x,y,0);
	Push(&h, temp);
	while (h.size)
	{
		temp = Pop(&h);
		x = temp.x;
		y = temp.y;

		if(time[x][y] < temp.cost) continue;
//		printf("V:[%d,%d; %d]\r\n", x, y, time[x][y]);
		for (int i = 0; i < 8; i += 2)
		{

			int8_t toX = x + avMoves[i];
			int8_t toY = y + avMoves[i+1];
			int16_t edgeWeight = AddWeight(prev[x][y], PII_(x,y), PII_(toX,toY));
//			printf("  paczam: [%d,%d; %d]\r\n", toX, toY, edgeWeight);
			if (time[toX][toY] > time[x][y] + edgeWeight && !(maze[toX][toY] & WALL))
			{
//				printf("  biere: [%d,%d; %d]\r\n", toX, toY, time[x][y]+edgeWeight);
				prev[toX][toY] = PII_(x,y);
				time[toX][toY] = time[x][y] + edgeWeight;
				Push(&h, Vertex_(toX, toY, edgeWeight));
			}
		}
	}
//	for (int i = mazeSizeCal - 1; i >= 0; --i){
//		for (int j = 0; j < mazeSizeCal; ++j)
//			printf("%5d ", time[j][i]);
//		printf("\r\n");
//	}

	printf("KONIEC\r\n");

	DetermineMoves(endX, endY, &tempOrient, mode);

//	int8_t x = startX;
//	int8_t y = startY;
//	int8_t tempOrient = (mode == BEST)? N : orientation;
//
//	for(int i=0; i<mazeSizeCal; i++)
//		for(int j=0; j<mazeSizeCal; j++)
//		{
//			time[i][j] = INF;
//			prev[i][j] = PII_(-1,-1);
//		}
//
//
//	time[x][y] = 0;
//	prev[x][y] = PII_(x,y);
//
//	for(int z=0; z<mazeSizeCal * mazeSizeCal - 1; z++)
//	{
//		int changeEx = 0;
//		for(int i=0; i<mazeSizeCal; i++)
//		{
//			for(int j=0; j<mazeSizeCal; j++)
//			{
//				if(time[i][j] == INF) continue;
//				for(int k=0; k<8; k+=2)
//				{
//					int8_t toX = i + avMoves[k];
//					int8_t toY = j + avMoves[k+1];
//					int16_t edgeWeight = AddWeight(prev[i][j], PII_(i,j), PII_(toX,toY));
//					if(time[toX][toY] > time[i][j] + edgeWeight && !(maze[toX][toY] & WALL))
//					{
//						changeEx = 1;
//						prev[toX][toY] = PII_(i,j);
//						time[toX][toY] = time[i][j] + edgeWeight;
//					}
//				}
//			}
//		}
//		if(!changeEx) break;
//	}
//	DetermineMoves(endX, endY, &tempOrient, mode);
}

void DetermineMoves (int8_t coordX, int8_t coordY, int8_t *orient, ModeNum mode)
{
	PII curField = PII_(coordX, coordY);
	PII prevField = prev[coordX][coordY];

	if(mode == BEST && !(maze[curField.ST][curField.ND] & CHECKED))
    {
		pointBest[pointBestCount] = curField;
		pointBestCount++;
	}
	if (prevField.ST == curField.ST && prevField.ND == curField.ND)
		return;

	DetermineMoves(prev[coordX][coordY].ST, prev[coordX][coordY].ND, &(*orient), mode); //*orient

	if ((maze[prevField.ST][prevField.ND] & CHECKED) && (mode == GOAL))
	{
		double vectorProduct = Det(curField.ST - prevField.ST, curField.ND - prevField.ND, avMoves[(*orient)*2], avMoves[(*orient)*2+1]);
		double scalarProduct = Dot(curField.ST - prevField.ST, curField.ND - prevField.ND, avMoves[(*orient)*2], avMoves[(*orient)*2+1]);

		if (vectorProduct == 0)
		{
			if (scalarProduct < 0)
			{
				printf("[180] ");
				ActionQInsert(Pivot180, 1);
				orient -= 2;
			}
		}

		if (scalarProduct == 0)
		{
			if (vectorProduct < 0)
			{
				printf("[90] ");
				ActionQInsert(Pivot, 1);
				//TODO: poprawic kierunek orientacji
				orient--;
			}
			else
			{
				printf("[-90] ");
				ActionQInsert(Pivot, -1);
				orient++;
			}
		}
		printf("[^] ");
		ActionQInsert(Move, 1);

		if (*orient > 3) orient -= 4;
		if (*orient < 0) orient += 4;

	}
}
