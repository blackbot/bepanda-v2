#ifndef HARDWARE_H
#define HARDWARE_H


/*****************************************************
 *  Settings
 ****************************************************/

/// LEDs =============================================
#define LED_RC_PORT				GPIOC
#define LED_RC_PIN				GPIO_Pin_0
#define LED_RC					LED_RC_PORT, LED_RC_PIN
#define LED_RC_INT				8

#define LED_RD_PORT				GPIOB
#define LED_RD_PIN				GPIO_Pin_12
#define LED_RD					LED_RD_PORT, LED_RD_PIN
#define LED_RD_INT				4

#define LED_LD_PORT				GPIOB
#define LED_LD_PIN				GPIO_Pin_13
#define LED_LD					LED_LD_PORT, LED_LD_PIN
#define LED_LD_INT				2

#define LED_LC_PORT				GPIOC
#define LED_LC_PIN				GPIO_Pin_6
#define LED_LC					LED_LC_PORT, LED_LC_PIN
#define LED_LC_INT				1

#define LED_LS_PORT				GPIOB
#define LED_LS_PIN				GPIO_Pin_15
#define LED_LS					LED_LS_PORT, LED_LS_PIN
#define LED_LS_INT				16


/// Buttons ==========================================
#define BUTTON1_PORT			GPIOB
#define BUTTON1_PIN				GPIO_Pin_8
#define BUTTON1					BUTTON1_PORT, BUTTON1_PIN

/// Buzzer ===========================================
#define BUZ_PWM_TIM				TIM1
#define BUZ_PWM_CHANNEL			TM_PWM_Channel_4
#define BUZ_PWM_PIN				TM_PWM_PinsPack_1

/// Battery Level ====================================
#define BAT_ADC					ADC1
#define BAT_CHANNEL				ADC_Channel_11
#define BAT						BAT_ADC, BAT_CHANNEL

/// Gyro =============================================
#define GYRO_I2C				I2C3
#define GYRO_PIN				TM_I2C_PinsPack_1
#define GYRO_FREQ				50000

#define GYRO_ADR				0b11010110
#define GYRO_WHO_AM_I			0b10001111
#define GYRO_WHO_AM_I_ANSWER	0b11010111
#define GYRO_CTRL_REG1			0b10100000
#define GYRO_CTRL_REG5			0b10100100
#define GYRO_OUT_Z_L			0b10101100
#define GYRO_OUT_Z_H			0b10101101

/// Display =========================================
#define DISPLAY_SPI				SPI2

#define DISPLAY_MOSI_PORT		GPIOC
#define DISPLAY_MOSI_PIN		GPIO_Pin_3

#define DISPLAY_SCK_PORT		GPIOB
#define DISPLAY_SCK_PIN			GPIO_Pin_10

#define DISPLAY_RS_PORT			GPIOC
#define DISPLAY_RS_PIN			GPIO_Pin_2
#define DISPLAY_RS				DISPLAY_RS_PORT, DISPLAY_RS_PIN

#define DISPLAY_CE_PORT			GPIOB
#define DISPLAY_CE_PIN			GPIO_Pin_9
#define DISPLAY_CE				DISPLAY_CE_PORT, DISPLAY_CE_PIN

/// TT ==============================================
#define TT_ADC					ADC1

#define TT_RC_CHANNEL			ADC_Channel_0
#define TT_RC					TT_ADC, TT_RC_CHANNEL

#define TT_RD_CHANNEL			ADC_Channel_2
#define TT_RD					TT_ADC, TT_RD_CHANNEL

#define TT_RS_CHANNEL			ADC_Channel_1
#define TT_RS					TT_ADC, TT_RS_CHANNEL

#define TT_LS_CHANNEL			ADC_Channel_8
#define TT_LS					TT_ADC, TT_LS_CHANNEL

#define TT_LD_CHANNEL			ADC_Channel_15
#define TT_LD					TT_ADC, TT_LD_CHANNEL

#define TT_LC_CHANNEL			ADC_Channel_9
#define TT_LC					TT_ADC, TT_LC_CHANNEL

/// IR ==============================================

#define IR_RC_PORT				GPIOA
#define IR_RC_PIN				GPIO_Pin_5
#define IR_RC					IR_RC_PORT, IR_RC_PIN

#define IR_RD_PORT				GPIOA
#define IR_RD_PIN				GPIO_Pin_3
#define IR_RD					IR_RD_PORT, IR_RD_PIN

#define IR_RS_PORT				GPIOA
#define IR_RS_PIN				GPIO_Pin_4
#define IR_RS					IR_RS_PORT, IR_RS_PIN

#define IR_LS_PORT				GPIOA
#define IR_LS_PIN				GPIO_Pin_7
#define	IR_LS					IR_LS_PORT, IR_LS_PIN

#define IR_LD_PORT				GPIOC
#define IR_LD_PIN				GPIO_Pin_4
#define IR_LD					IR_LD_PORT, IR_LD_PIN

#define IR_LC_PORT				GPIOA
#define IR_LC_PIN				GPIO_Pin_6
#define IR_LC					IR_LC_PORT, IR_LC_PIN

#define IR_GAIN_PORT			GPIOC
#define IR_GAIN_PIN				GPIO_Pin_8
#define IR_GAIN					IR_GAIN_PORT, IR_GAIN_PIN

/// Motors ==========================================
#define MOTOR_L1_PORT			GPIOC
#define MOTOR_L1_PIN			GPIO_Pin_11
#define MOTOR_L1				MOTOR_L1_PORT, MOTOR_L1_PIN

#define MOTOR_L2_PORT			GPIOC
#define MOTOR_L2_PIN			GPIO_Pin_10
#define MOTOR_L2				MOTOR_L2_PORT, MOTOR_L2_PIN

#define MOTOR_R1_PORT			GPIOD
#define MOTOR_R1_PIN			GPIO_Pin_2
#define MOTOR_R1				MOTOR_R1_PORT, MOTOR_R1_PIN

#define MOTOR_R2_PORT			GPIOC
#define MOTOR_R2_PIN			GPIO_Pin_12
#define MOTOR_R2				MOTOR_R2_PORT, MOTOR_R2_PIN

/// MOTOR_PWM=============================================
#define PWM_TIMER				TIM3
#define PWM_FREQ				20000

#define PWM_L_CHANNEL			TM_PWM_Channel_1
#define PWM_L_PIN				TM_PWM_PinsPack_2

#define PWM_R_CHANNEL			TM_PWM_Channel_2
#define PWM_R_PIN				TM_PWM_PinsPack_2

// Encoders ========================================
// Left Motor Channels
#define ENCLA_PORT        		GPIOA
#define ENCLA_PIN               GPIO_Pin_15
#define ENCLA					ENCLA_PORT, ENCLA_PIN

#define ENCLB_PORT         		GPIOB
#define ENCLB_PIN               GPIO_Pin_3
#define ENCLB					ENCLB_PORT, ENCLB_PIN

// Right Motor Channels
#define ENCRA_PORT         		GPIOB
#define ENCRA_PIN               GPIO_Pin_6
#define ENCRA					ENCRA_PORT, ENCRA_PIN

#define ENCRB_PORT         		GPIOB
#define ENCRB_PIN               GPIO_Pin_7
#define ENCRB					ENCRB_PORT, ENCRB_PIN

// determine the timers to use
#define ENCL_TIMER              TIM2
#define ENCL_TIMER_GPIO			GPIO_AF_TIM2
#define ENCL_TIMER_CLK          RCC_APB1Periph_TIM2

#define ENCR_TIMER              TIM4
#define ENCR_TIMER_GPIO			GPIO_AF_TIM4
#define ENCR_TIMER_CLK          RCC_APB1Periph_TIM4

// The serial port ==============================

#define USART_BAUD          38400
#define USART               USART1
#define USART_CLK           RCC_APB2Periph_USART1
#define USART_CLK_CMD       RCC_APB2PeriphClockCmd

#define USART_GPIO          GPIOA
#define USART_GPIO_CLK      RCC_AHB1Periph_GPIOA
#define USART_GPIO_CLK_CMD  RCC_AHB1PeriphClockCmd
#define GPIO_AF_USART       GPIO_AF_USART1
#define USART_TX_PIN       	GPIO_Pin_9
#define USART_RX_PIN       	GPIO_Pin_10
#define USART_TX_AF_SRC     GPIO_PinSource9
#define USART_RX_AF_SRC     GPIO_PinSource10

#define USART_IRQn                  USART1_IRQn
#define USARTx_IRQHANDLER 		 	USART1_IRQHandler

#endif
