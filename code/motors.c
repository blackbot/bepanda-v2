#include "main.h"

TM_PWM_TIM_t PWMTimerData;


void Motors_Init()
{
	// MOTORS ===============================================
	TM_GPIO_Init(MOTOR_L1, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(MOTOR_L2, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(MOTOR_R1, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(MOTOR_R2, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);

	// PWM =============================================

	TM_PWM_InitTimer(PWM_TIMER, &PWMTimerData, PWM_FREQ);

	TM_PWM_InitChannel(&PWMTimerData, PWM_L_CHANNEL, PWM_L_PIN);
	TM_PWM_InitChannel(&PWMTimerData, PWM_R_CHANNEL, PWM_R_PIN);

	TM_PWM_SetChannelPercent(&PWMTimerData, PWM_L_CHANNEL, 0);
	TM_PWM_SetChannelPercent(&PWMTimerData, PWM_R_CHANNEL, 0);

}

void Motors(int32_t speedLeft, int32_t speedRight)
{
	// DIRECTION ===========================================
	if(speedLeft < 0)
	{
		TM_GPIO_SetPinHigh(MOTOR_L1);
		TM_GPIO_SetPinLow(MOTOR_L2);
	}
	else
	{
		TM_GPIO_SetPinLow(MOTOR_L1);
		TM_GPIO_SetPinHigh(MOTOR_L2);
	}

	if(speedRight < 0)
	{
		TM_GPIO_SetPinHigh(MOTOR_R1);
		TM_GPIO_SetPinLow(MOTOR_R2);
	}
	else
	{
		TM_GPIO_SetPinLow(MOTOR_R1);
		TM_GPIO_SetPinHigh(MOTOR_R2);
	}

	// Proper Value =======================================
	speedLeft = (speedLeft > 100)? 100 : abs(speedLeft);
	speedRight = (speedRight > 100)? 100 : abs(speedRight);

	// Set ================================================
	TM_PWM_SetChannelPercent(&PWMTimerData, PWM_L_CHANNEL, speedLeft);
	TM_PWM_SetChannelPercent(&PWMTimerData, PWM_R_CHANNEL, speedRight);
}


void Encoders_Init (void)
{

	TM_GPIO_Init(LED_LC, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);

	TM_GPIO_InitAlternate(ENCLA, TM_GPIO_OType_PP, TM_GPIO_PuPd_UP,  TM_GPIO_Speed_High, ENCL_TIMER_GPIO);
	TM_GPIO_InitAlternate(ENCLB, TM_GPIO_OType_PP, TM_GPIO_PuPd_UP,  TM_GPIO_Speed_High, ENCL_TIMER_GPIO);
	TM_GPIO_InitAlternate(ENCRA, TM_GPIO_OType_PP, TM_GPIO_PuPd_UP,  TM_GPIO_Speed_High, ENCR_TIMER_GPIO);
	TM_GPIO_InitAlternate(ENCRB, TM_GPIO_OType_PP, TM_GPIO_PuPd_UP,  TM_GPIO_Speed_High, ENCR_TIMER_GPIO);



	// Timer peripheral clock enable
	RCC_APB1PeriphClockCmd (ENCL_TIMER_CLK, ENABLE);
	RCC_APB1PeriphClockCmd (ENCR_TIMER_CLK, ENABLE);

	// set them up as encoder inputs
	// set both inputs to rising polarity to let it use both edges
	TIM_EncoderInterfaceConfig (ENCL_TIMER, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_SetAutoreload (ENCL_TIMER, 0xffff);
	TIM_EncoderInterfaceConfig (ENCR_TIMER, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_SetAutoreload (ENCR_TIMER, 0xffff);

	// turn on the timer/counters
	TIM_Cmd (ENCL_TIMER, ENABLE);
	TIM_Cmd (ENCR_TIMER, ENABLE);
}
