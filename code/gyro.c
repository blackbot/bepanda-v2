#include "main.h"

double data;
double angleZ;

int8_t gyroInit;
int16_t count;
int16_t calibrationCount;
const int16_t calibrationCountMax = 1000;

double bias;
double biasInit;

const double GYRO_SENSITIVITY_250DPS  = 0.00875; 	// 8.75 mdps/digit
const double GYRO_SENSITIVITY_500DPS  = 0.0175;		//17.50 mdps/digit
const double GYRO_SENSITIVITY_2000DPS = 0.070;		//70.00 mdps/digit

void Gyro_Init()
{
	TM_I2C_Init(GYRO_I2C, GYRO_PIN, GYRO_FREQ);
	Delayms(1);
	//turn GYRO on
	TM_I2C_Write(GYRO_I2C, GYRO_ADR, GYRO_CTRL_REG1, 0x08);
	Delayms(2);
	//reboot GYRO
	TM_I2C_Write(GYRO_I2C, GYRO_ADR, GYRO_CTRL_REG5, 0x80);
	Delayms(1);
	TM_I2C_Write(GYRO_I2C, GYRO_ADR, GYRO_CTRL_REG5, 0x00);
	Delayms(1);

	//write registers
	uint8_t buff[4] = { 0xFC, 0x00, 0x00, 0x00};
	TM_I2C_WriteMulti(GYRO_I2C, GYRO_ADR, GYRO_CTRL_REG1, buff, 4);
	Delayms(1);

	//check response
	uint8_t reply = TM_I2C_Read(GYRO_I2C, GYRO_ADR, GYRO_WHO_AM_I);
	Delayms(100);
	//printf("[%d]\n", reply);

	//TM_GPIO_Init(GPIOB, GPIO_Pin_4, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_2MHz);
	//TM_GPIO_Init(GPIOB, GPIO_Pin_5, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_2MHz);
	if (reply == GYRO_WHO_AM_I_ANSWER){
		gyroInit = 1;
		//TM_GPIO_SetPinHigh(GPIOB, GPIO_Pin_4);
		//printf("Gyro init exit with success.\n");
		SetLed(LED_LS, ON);
		Delayms(500);
		SetLed(LED_LS, OFF);
		Delayms(500);
	}
	else{
		//TM_GPIO_SetPinHigh(GPIOB, GPIO_Pin_5);
		//printf("Gyro init failed.\n");
	}
}

void GyroUpdate(void)
{
	if (!gyroInit) return;
	
	count++;
	if ((count & 1) == 0){
		count = 0;
		return;
	}
	
	int16_t dataZ;
	dataZ  = TM_I2C_Read(GYRO_I2C, GYRO_ADR, GYRO_OUT_Z_L);
	dataZ |= TM_I2C_Read(GYRO_I2C, GYRO_ADR, GYRO_OUT_Z_H) << 8;
	data = (float)dataZ * GYRO_SENSITIVITY_250DPS * 0.002; //0.002 * 9;

	data -= bias;

	if (calibrationCount <= calibrationCountMax){
		calibrationCount++;
		biasInit += data;
		if (calibrationCount == calibrationCountMax)
			bias = biasInit/calibrationCountMax;
	}
	else{
		angleZ += data;
	}
}

double GyroGetAngle (void)
{
	return angleZ;
}

void GyroZero(void)
{
	angleZ = 0;
}

