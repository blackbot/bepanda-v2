#ifndef MOTORS_H
#define MOTORS_H


void Motors_Init(void);
void Motors(int32_t speedLeft, int32_t speedRight);
void Encoders_Init(void);

#endif
