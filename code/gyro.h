#ifndef GYRO_H
#define GYRO_H

void Gyro_Init(void);
void GyroUpdate(void);
double GyroGetAngle(void);
void GyroZero (void);

#endif //GYRO_H
