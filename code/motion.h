#ifndef MOTION_H
#define MOTION_H

#include "enums.h"

extern int16_t wallAppearance[7][7];

extern double curSpeedX;
extern double curSpeedW;
extern double targetSpeedX;
extern double targetSpeedW;

extern int32_t leftEncoderCount;
extern int32_t rightEncoderCount;

extern double accX;
extern double decX;
extern double accW;
extern double decW;

extern double kpX, kdX, kpW, kdW;

extern double T1, T2, T3;

extern int32_t distanceLeft;
extern int32_t tempPos;
int8_t centerWallAlign;

extern const int32_t FIELD_DISTANCE;

extern FeedbackSourceNum feedbackSource;

typedef struct{
	//X:
	int16_t speedX; //[mm/s]
	double accX;	//[mm/s/s]
	double decX;

	double kpX;
	double kdX;

	//W:
	int16_t speedW; //[deg/s]
	double accW; 	//[deg/s/s]
	double decW;

	double kpW;
	double kdW;

	double T1;		//[ms]
	double T2;
	double T3;
}SetupPack;

extern SetupPack setup[10][10];

double SpeedToTicks(int16_t x);
int32_t TicksToSpeed(int32_t x);
double DegToTicks(int16_t x);
double NeedToDecelerate(int32_t dist, double curSpd, double endSpd);
void ControlLoop(void);
void ResetAll(void);

#endif
