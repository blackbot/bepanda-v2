#include "main.h"

//      Other variables
/*-----------------------------*/
const int32_t FIELD_DISTANCE = 38820; // wartosc dla 180mm const int32_t FIELD_DISTANCE = 77640;
const int32_t FIELD_LENGTH = 84; //dla std pola jest 168mm
const double CRIT_BAT_LVL = 7;
const int16_t WALL_AVAILABLE = 150;


int8_t fUseController = OFF;

FeedbackSourceNum feedbackSource;
RunFlagNum runFlag;

//PID
int32_t distanceLeftCutOff = 400; //[ticks]

//IR Distances
int32_t LSWall_1[2] = {22, 65};
int32_t LSWall_2[2] = {180, 250};
int32_t RSWall_1[2] = {25, 75};
int32_t RSWall_2[2] = {170, 270};

int32_t LDWall_1[2] = {35, 90};
int32_t LDWall_2[2] = {105, 150};
int32_t RDWall_1[2] = {30, 85};
int32_t RDWall_2[2] = {110, 160};

int32_t centerWall_1;
int32_t centerWall_2;
int32_t centerVisionDistance;
int32_t centerStickingDistance;

int32_t diagonalVisionDistance;
int32_t diagonalStickingDistance;

void Config()
{
	//TODO: temporary
	feedbackSource = EncSens;
	runFlag = SearchRun;

	//****************
	//**     IR     **
	//** DISTANCES  **
	//****************
	//Center
	centerWall_1 = 0.75 * FIELD_LENGTH;
	centerWall_2 = 2.1 * FIELD_LENGTH;
	centerVisionDistance = 80;
	centerStickingDistance = 53; //38; //[mm]
	diagonalVisionDistance = FIELD_LENGTH;
	diagonalStickingDistance = 50; //[mm]

	//****************
	//**   SEARCH   **
	//**    RUN     **
	//****************

	// ********************************** Move (-)
	setup[SearchRun][Move].speedX = 500;
	setup[SearchRun][Move].accX = 2000;
	setup[SearchRun][Move].decX = 2000;
	setup[SearchRun][Move].kpX = 0.1;
	setup[SearchRun][Move].kdX = 0;

	setup[SearchRun][Move].speedW = 0;
	setup[SearchRun][Move].accW = 0;
	setup[SearchRun][Move].decW = 0;
	setup[SearchRun][Move].kpW = 0.1;
	setup[SearchRun][Move].kdW = 0;

	setup[SearchRun][Move].T1 = 0;
	setup[SearchRun][Move].T2 = 0;
	setup[SearchRun][Move].T3 = 0;


	// ********************************** Pivot (-)
	setup[SearchRun][Pivot].speedX = 0;
	setup[SearchRun][Pivot].accX = 2000;
	setup[SearchRun][Pivot].decX = 2000;
	setup[SearchRun][Pivot].kpX = 0.1;
	setup[SearchRun][Pivot].kdX = 0;

	setup[SearchRun][Pivot].speedW = 300;
	setup[SearchRun][Pivot].accW = 3000;
	setup[SearchRun][Pivot].decW = 3000;
	setup[SearchRun][Pivot].kpW = 0.1;
	setup[SearchRun][Pivot].kdW = 0.3;

	setup[SearchRun][Pivot].T1 = 100;
	setup[SearchRun][Pivot].T2 = 240; //powinno by� 200
	setup[SearchRun][Pivot].T3 = 100;

	// ********************************** Pivot180 (-)
	setup[SearchRun][Pivot180].speedX = 0;
	setup[SearchRun][Pivot180].accX = 2000;
	setup[SearchRun][Pivot180].decX = 2000;
	setup[SearchRun][Pivot180].kpX = 0.1;
	setup[SearchRun][Pivot180].kdX = 0;

	setup[SearchRun][Pivot180].speedW = 300;
	setup[SearchRun][Pivot180].accW = 3000;
	setup[SearchRun][Pivot180].decW = 3000;
	setup[SearchRun][Pivot180].kpW = 0.1;
	setup[SearchRun][Pivot180].kdW = 0.3;

	setup[SearchRun][Pivot180].T1 = 100;
	setup[SearchRun][Pivot180].T2 = 500;
	setup[SearchRun][Pivot180].T3 = 100;


	// ********************************** TurnSmooth (-)
	setup[SearchRun][TurnSmooth].speedX = 400;
	setup[SearchRun][TurnSmooth].accX = 2000;
	setup[SearchRun][TurnSmooth].decX = 2000;
	setup[SearchRun][TurnSmooth].kpX = 0.1;
	setup[SearchRun][TurnSmooth].kdX = 0;

	setup[SearchRun][TurnSmooth].speedW = 330;
	setup[SearchRun][TurnSmooth].accW = 3500;
	setup[SearchRun][TurnSmooth].decW = 3500;
	setup[SearchRun][TurnSmooth].kpW = 0.1;
	setup[SearchRun][TurnSmooth].kdW = 1.0;

	setup[SearchRun][TurnSmooth].T1 = 94;
	setup[SearchRun][TurnSmooth].T2 = 179; //100
	setup[SearchRun][TurnSmooth].T3 = 94;

	// ********************************** Calibrate (-)
	///TODO: nada� wsp�czynniki
	setup[SearchRun][Calibrate].speedX = 0;
	setup[SearchRun][Calibrate].accX = 0;
	setup[SearchRun][Calibrate].decX = 0;
	setup[SearchRun][Calibrate].kpX = 0.1;
	setup[SearchRun][Calibrate].kdX = 0;

	setup[SearchRun][Calibrate].speedW = 90;
	setup[SearchRun][Calibrate].accW = 500;
	setup[SearchRun][Calibrate].decW = 500;
	setup[SearchRun][Calibrate].kpW = 0.1;
	setup[SearchRun][Calibrate].kdW = 0;

	setup[SearchRun][Calibrate].T1 = 180;
	setup[SearchRun][Calibrate].T2 = 820;
	setup[SearchRun][Calibrate].T3 = 180;


	//****************
	//**   FINAL    **
	//**    RUN     **
	//****************

	// ********************************** Move (-)
	setup[FinalRun][Move].speedX = 500;
	setup[FinalRun][Move].accX = 2000;
	setup[FinalRun][Move].decX = 2000;
	setup[FinalRun][Move].kpX = 0.1;
	setup[FinalRun][Move].kdX = 0;

	setup[FinalRun][Move].speedW = 0;
	setup[FinalRun][Move].accW = 0;
	setup[FinalRun][Move].decW = 0;
	setup[FinalRun][Move].kpW = 0.1;
	setup[FinalRun][Move].kdW = 0;

	setup[FinalRun][Move].T1 = 0;
	setup[FinalRun][Move].T2 = 0;
	setup[FinalRun][Move].T3 = 0;


	// ********************************** Pivot (-)
	setup[FinalRun][Pivot].speedX = 0;
	setup[FinalRun][Pivot].accX = 0;
	setup[FinalRun][Pivot].decX = 0;
	setup[FinalRun][Pivot].kpX = 0.1;
	setup[FinalRun][Pivot].kdX = 0;

	setup[FinalRun][Pivot].speedW = 0;
	setup[FinalRun][Pivot].accW = 0;
	setup[FinalRun][Pivot].decW = 0;
	setup[FinalRun][Pivot].kpW = 0.1;
	setup[FinalRun][Pivot].kdW = 0;

	setup[FinalRun][Pivot].T1 = 0;
	setup[FinalRun][Pivot].T2 = 0;
	setup[FinalRun][Pivot].T3 = 0;

	// ********************************** Pivot180 (-)
	setup[FinalRun][Pivot180].speedX = 0;
	setup[FinalRun][Pivot180].accX = 0;
	setup[FinalRun][Pivot180].decX = 0;
	setup[FinalRun][Pivot180].kpX = 0.1;
	setup[FinalRun][Pivot180].kdX = 0;

	setup[FinalRun][Pivot180].speedW = 0;
	setup[FinalRun][Pivot180].accW = 0;
	setup[FinalRun][Pivot180].decW = 0;
	setup[FinalRun][Pivot180].kpW = 0.1;
	setup[FinalRun][Pivot180].kdW = 0;

	setup[FinalRun][Pivot180].T1 = 0;
	setup[FinalRun][Pivot180].T2 = 0;
	setup[FinalRun][Pivot180].T3 = 0;


	// ********************************** TurnSmooth (-)
	setup[FinalRun][TurnSmooth].speedX = 0;
	setup[FinalRun][TurnSmooth].accX = 0;
	setup[FinalRun][TurnSmooth].decX = 0;
	setup[FinalRun][TurnSmooth].kpX = 0.1;
	setup[FinalRun][TurnSmooth].kdX = 0;

	setup[FinalRun][TurnSmooth].speedW = 0;
	setup[FinalRun][TurnSmooth].accW = 0;
	setup[FinalRun][TurnSmooth].decW = 0;
	setup[FinalRun][TurnSmooth].kpW = 0.1;
	setup[FinalRun][TurnSmooth].kdW = 0;

	setup[FinalRun][TurnSmooth].T1 = 0;
	setup[FinalRun][TurnSmooth].T2 = 0;
	setup[FinalRun][TurnSmooth].T3 = 0;

	// ********************************** Calibrate (-)
	setup[FinalRun][Calibrate].speedX = 0;
	setup[FinalRun][Calibrate].accX = 0;
	setup[FinalRun][Calibrate].decX = 0;
	setup[FinalRun][Calibrate].kpX = 0.1;
	setup[FinalRun][Calibrate].kdX = 0;

	setup[FinalRun][Calibrate].speedW = 0;
	setup[FinalRun][Calibrate].accW = 0;
	setup[FinalRun][Calibrate].decW = 0;
	setup[FinalRun][Calibrate].kpW = 0.1;
	setup[FinalRun][Calibrate].kdW = 0;

	setup[FinalRun][Calibrate].T1 = 0;
	setup[FinalRun][Calibrate].T2 = 0;
	setup[FinalRun][Calibrate].T3 = 0;
}
