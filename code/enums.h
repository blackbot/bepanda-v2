#ifndef ENUMS_H
#define ENUMS_H

typedef enum
{
	ON,
	OFF,
	TOGGLE
} StateNum;

typedef enum
{
	CMD,
	DATA,
	INT,
	CLEAR,
	ERR
} DisplayTypeNum;


typedef enum
{
	Encoder,
	EncSens,
	EncGyro,
	Gyro
} FeedbackSourceNum;

typedef enum
{
	Move,
	Pivot,
	Pivot180,
	TurnSmooth,
	Calibrate
} ActionTypeNum;

typedef enum
{
	DONE = 0,
	TODO,
	PROCESSING,

} ActionStatusNum;

typedef enum
{
	SearchRun,
	FinalRun,
	ReturnRun
} RunFlagNum;

typedef enum
{
	N,
	E,
	S,
	W
} OrientationNum;

typedef enum
{
	BEST,
	MY_POS,
	GOAL
} ModeNum;

#endif
