#include "main.h"

#define TOP 0x08
#define RIGHT 0x04
#define BOTTOM 0x02
#define LEFT 0x01
#define MOUSE 0x10

int16_t out[18][18];
void PrintfMaze()
{
    for(int i = 0; i < mazeSizeCal; i++)
    {
        for(int j = 0; j < mazeSizeCal; j++)
        {
            if((i&1) && (j&1))
            {
                int toX = (i)/2;
                int toY = (j)/2;

                if(maze[i][j-1] & WALLCHECKED)
                    out[toX][toY] |= BOTTOM;
                if(maze[i][j+1] & WALLCHECKED)
                    out[toX][toY] |= TOP;
                if(maze[i-1][j] & WALLCHECKED)
                    out[toX][toY] |= LEFT;
                if(maze[i+1][j] & WALLCHECKED)
                    out[toX][toY] |= RIGHT;
            }
        }
    }


    for(int i = 0; i < 16; i++)
    {
        for(int j = 0; j < 16; j++){
            printf("ST 0 %d %d %d EN\n", i+1,j+1,out[i][j]);
            Delayms(50);
        }
    }
}

void PrintfCell(int8_t x, int8_t y)
{
	for(int i = 0; i < mazeSizeCal; i++)
	{
		for(int j = 0; j < mazeSizeCal; j++)
		{
			if((i&1) && (j&1))
			{
				int toX = (i)/2;
				int toY = (j)/2;

				if(maze[i][j-1] & WALLCHECKED)
					out[toX][toY] |= BOTTOM;
				if(maze[i][j+1] & WALLCHECKED)
					out[toX][toY] |= TOP;
				if(maze[i-1][j] & WALLCHECKED)
					out[toX][toY] |= LEFT;
				if(maze[i+1][j] & WALLCHECKED)
					out[toX][toY] |= RIGHT;
			}
		}
	}
	printf("ST 0 %d %d %d EN\n", x/2+1,y/2+1,out[x/2][y/2]);
	Delayms(50);
}
