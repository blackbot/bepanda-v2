#include "main.h"

actionPack actionQ[ACTIONQ_SIZE];
uint16_t qR, qW;
uint16_t actionQSize;

void swap(int16_t *a, int16_t *b)
{
	int16_t pom = *a;
	*a = *b;
	*b = pom;
}

void ActionQInsert (ActionTypeNum type, double arg)
{
	actionQSize++;
	actionQ[qW].type = type;
	actionQ[qW].status = TODO;
	switch (type)
	{
		case Move:
			actionQ[qW].arg = arg;
			break;
		case Pivot:
			actionQ[qW].arg = (arg > 0)? 1 : -1;
			break;
		case Pivot180:
			actionQ[qW].arg = (arg > 0)? 1 : -1;
			break;
		case TurnSmooth:
			actionQ[qW].arg = (arg > 0)? 1 : -1;
			break;
		case Calibrate:
			//dummy arg
			break;
	}
	qW = (qW+1) & (ACTIONQ_SIZE - 1);
}

void ActionQProcess ()
{
	switch (actionQ[qR].status)
	{
		case TODO:
			//ustawienie danych wejciowych dla regulatora:
			actionQ[qR].timeStamp = TM_DELAY_Time();
			actionQ[qR].status = PROCESSING;

			targetSpeedX = SpeedToTicks(setup[runFlag][actionQ[qR].type].speedX);
			accX = setup[runFlag][actionQ[qR].type].accX;
			decX = setup[runFlag][actionQ[qR].type].decX;
			kpX  = setup[runFlag][actionQ[qR].type].kpX;
			kdX  = setup[runFlag][actionQ[qR].type].kdX;

			targetSpeedW = DegToTicks(setup[runFlag][actionQ[qR].type].speedW * actionQ[qR].arg);
			accW = setup[runFlag][actionQ[qR].type].accW;
			decW = setup[runFlag][actionQ[qR].type].decW;
			kpW  = setup[runFlag][actionQ[qR].type].kpW;
			kdW  = setup[runFlag][actionQ[qR].type].kdW;

			T1 = setup[runFlag][actionQ[qR].type].T1;
			T2 = setup[runFlag][actionQ[qR].type].T2;
			T3 = setup[runFlag][actionQ[qR].type].T3;

			centerWallAlign = OFF;

			switch (actionQ[qR].type)
			{
				case Move:
					DisplayData("Move");
					distanceLeft = actionQ[qR].arg * FIELD_DISTANCE;
					break;
				case Pivot:
					DisplayData("T90");
					GyroZero();
					SetLed(LED_LS, OFF);
					break;
				case Pivot180:
					DisplayData("T180");
					break;
				case TurnSmooth:
					DisplayData("TS");
					distanceLeft = 2*150*215;
					break;
				case Calibrate:
					DisplayData("Clbt");
					///TODO: cos tu trzeba dopisac, jakas mala predkosc
					break;
			}
			break;
		case PROCESSING:
			//wszelkie przetwarzanie aktualnego ruchu jest w motion.c w funckji CalculateParams()
			break;
		case DONE:
			qR++;
			qR &= ACTIONQ_SIZE - 1;
			break;
	}
}

void ActionQActionDone(uint16_t i)
{
	DisplayData("DONE");
	SetLed(LED_LS, ON);
	actionQ[i].status = DONE;
	actionQSize--;

	printf("AQS:<%d>\r\n", actionQSize);

	//TODO: kolejkowanie ostatnich 10 zmienionych

	//********************************************************************************************************
	//TODO: przepisanie scianek do mapy
//	printf("wystapienia scianek:\r\n");
//	for (int i = 0; i < 7; ++i)
//	{
//		for (int j = 0; j < 7; ++j)
//			printf("%4d", wallAppearance[i][j]);
//		printf("\r\n");
//	}
//	printf("************\r\n");

	int8_t fWallAppearance = 0;
	int8_t coordWalls[6] = {1,-2, 2,-2, 2,-3};

	int16_t temp[7][7];
	for (int i = 0; i < 7; ++i)
			for (int j = 0; j < 7; ++j)
				temp[i][j] = 0;

	//wsp rozpoczecia ruchu
	int8_t tx = 5;
	int8_t ty = 3;

	//scianki do przodu
	for (int i = 1; i <= 3; ++i)
	{
		temp[tx-i][ty] |= CHECKED;
		if (wallAppearance[tx-i][ty] > WALL_THRESHOLD)
		{
			temp[tx-i][ty] |= WALLCHECKED;
			fWallAppearance = 1;
			//printf("Przod %d\r\n", i);
			break;
		}
	}

	//scianki w lewo
	for (int i = 1; i <= 3; ++i)
	{
		temp[tx][ty-i] |= CHECKED;
		if (wallAppearance[tx][ty-i] > WALL_THRESHOLD)
		{
			temp[tx][ty-i] |= WALLCHECKED;
			fWallAppearance = 1;
			//printf("Lewo %d\r\n", i);
			break;
		}
	}

	//scianki w prawo
	for (int i = 1; i <= 3; ++i)
	{
		temp[tx][ty+i] |= CHECKED;
		if (wallAppearance[tx][ty+i] > WALL_THRESHOLD)
		{
			temp[tx][ty+i] |= WALLCHECKED;
			fWallAppearance = 1;
			//printf("Prawo %d\r\n", i);
			break;
		}
	}
	//scianki lewo skos
	for (int i = 0; i < 6; i += 2)
	{
		int8_t toX = tx + coordWalls[i+1];
		int8_t toY = ty - coordWalls[i];
		temp[toX][toY] |= CHECKED;
		if (wallAppearance[toX][toY] > WALL_THRESHOLD)
		{
			temp[toX][toY] |= WALLCHECKED;
			fWallAppearance = 1;
			//printf("Lewo skos %d\r\n", i);
			break;
		}
	}

	//scianki prawo skos
	for (int i = 0; i < 6; i += 2)
	{
		int8_t toX = tx + coordWalls[i+1];
		int8_t toY = ty + coordWalls[i];
		temp[toX][toY] |= CHECKED;
		if (wallAppearance[toX][toY] > WALL_THRESHOLD)
		{
			temp[toX][toY] |= WALLCHECKED;
			fWallAppearance = 1;
			//printf("Prawo skos %d\r\n", i);
			break;
		}
	}

	switch (orientation)
	{
		case N: //do nothing
			break;
		case E:
			//trans + flip po y
			//transpose
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j < 7; ++j)
					if (i>j)
						swap(&temp[i][j], &temp[j][i]);
			//flip po y
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j <3; ++j)
					swap(&temp[i][j], &temp[i][7-j-1]);
			break;
		case S:
			//flip po x, flip po y
			//flip po x
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j <3; ++j)
					swap(&temp[j][i], &temp[7-j-1][i]);
			//flip po y
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j <3; ++j)
					swap(&temp[i][j], &temp[i][7-j-1]);
			break;
		case W:
			//transpose
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j < 7; ++j)
					if (i>j)
						swap(&temp[i][j], &temp[j][i]);
			//flip po x
			for (int i = 0; i < 7; ++i)
				for (int j = 0; j <3; ++j)
					swap(&temp[j][i], &temp[7-j-1][i]);
			break;
	}

	temp[tx][ty] |= CHECKED;
	for (int i = -6; i <= 6; ++i){
		for (int j = -6; j <= 6; ++j)
			if (posX+i >= 0 && posX+i < mazeSizeCal &&
				posY+j >= 0 && posY+j < mazeSizeCal &&
				tx-i >= 0 && tx-i < 7 &&
				ty+j   >= 0 && ty+j < 7)
			{
				//printf("[%d,%d]vs{%d,%d}\r\n", posX+i, posY+j, tx+i, ty+j);
				maze[posX+i][posY+j] |= temp[tx-i][ty+j];
				if (temp[tx-i][ty+j] & WALL)
					printf("Mam scianke %d,%d\r\n", posX+i, posY+j);
			}
	}

	//TODO: zerowanie wallAppearance
	for (int i = 0; i < 7; ++i)
		for (int j = 0; j < 7; ++j)
			wallAppearance[i][j] = 0;

	//********************************************************************************************************
	///TODO: tutaj aktualizacja pola, w kt�rym jestem
	//przejscie do kolejnej komorki
	switch (actionQ[qR].type)
	{
	case Move:
		posX += avMoves[orientation];
		posY += avMoves[orientation+1];
		break;
	case Pivot:
		orientation += (-1) * actionQ[i].arg;
		break;
	case Pivot180:
		orientation += (-2) * actionQ[i].arg;
		break;
	case TurnSmooth:
		posX += avMoves[orientation];
		posY += avMoves[orientation+1];
		orientation += (-1) * actionQ[i].arg;
		if (orientation < 0) orientation += 4;
		if (orientation > 3) orientation -= 4;
		posX += avMoves[orientation];
		posY += avMoves[orientation+1];
		break;
	case Calibrate:
		//nothing to be done
		break;
	}
	if (orientation < 0) orientation += 4;
	if (orientation > 3) orientation -= 4;

	//PrintfCell(posX, posY);

	//********************************************************************************************************
	if (actionQSize == 0 || fWallAppearance == 1)
	{
		fAlgorithm = ON;
	}
}

