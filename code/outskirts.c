#include "main.h"
#include "misc.h"


TM_PWM_TIM_t PWMTimerData;
int FunnyLedsTab[20] = {
						OFF,
						LED_LC_INT,
						LED_LC_INT | LED_LD_INT,
						LED_LC_INT | LED_LD_INT | LED_RD_INT,
						LED_LC_INT | LED_LD_INT | LED_RD_INT | LED_RC_INT,
									 LED_LD_INT | LED_RD_INT | LED_RC_INT,
									              LED_RD_INT | LED_RC_INT,
									                           LED_RC_INT,
									              LED_RD_INT | LED_RC_INT,
									 LED_LD_INT | LED_RD_INT | LED_RC_INT,
						LED_LC_INT | LED_LD_INT | LED_RD_INT | LED_RC_INT,
						LED_LC_INT | LED_LD_INT | LED_RD_INT,
						LED_LC_INT | LED_LD_INT,
						LED_LC_INT,
						OFF
					};

int16_t buzzerTime;

void Outskirts_Init()
{
	// LEDs =======================================
	TM_GPIO_Init(LED_LC, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(LED_LS, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(LED_LD, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(LED_RD, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);
	TM_GPIO_Init(LED_RC, TM_GPIO_Mode_OUT, TM_GPIO_OType_PP, TM_GPIO_PuPd_NOPULL, TM_GPIO_Speed_High);

	// Buttons ====================================
	TM_BUTTON_Init(BUTTON1, 0, BUTTON1_EventHandler);	// (0) means low when pressed

	// Battery Level ==============================
	TM_ADC_Init(BAT);
}


void SetLed(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, uint8_t x)
{
	if(x == ON)
		TM_GPIO_SetPinHigh(GPIOx, GPIO_Pin);
	else
		TM_GPIO_SetPinLow(GPIOx, GPIO_Pin);
} 

void SetLedInt(uint8_t x)
{
	if(x & LED_LC_INT) TM_GPIO_SetPinHigh(LED_LC);
	else TM_GPIO_SetPinLow(LED_LC);

	if(x & LED_LD_INT) TM_GPIO_SetPinHigh(LED_LD);
		else TM_GPIO_SetPinLow(LED_LD);

	if(x & LED_RD_INT) TM_GPIO_SetPinHigh(LED_RD);
		else TM_GPIO_SetPinLow(LED_RD);

	if(x & LED_RC_INT) TM_GPIO_SetPinHigh(LED_RC);
		else TM_GPIO_SetPinLow(LED_RC);
}

void FunnyLeds()
{

	for(int i=0; i<15; i++)
	{
		SetLedInt(FunnyLedsTab[i]);
		Delayms(100);
	}
/*
	for(int i=0; i<2; i++)
	{
		Delayms(500);
		SetLedInt( LED_LC_INT | LED_LD_INT | LED_RD_INT | LED_RC_INT );
		Delayms(500);
		SetLedInt(OFF);
	}*/
}

void Buzzer(int16_t freq, int16_t duration)
{
	TM_PWM_InitTimer(BUZ_PWM_TIM, &PWMTimerData, freq);
	TM_PWM_InitChannel(&PWMTimerData, BUZ_PWM_CHANNEL, BUZ_PWM_PIN);
	TM_PWM_SetChannelPercent(&PWMTimerData, BUZ_PWM_CHANNEL, 0);

	buzzerTime = duration;
}


void CheckBuzzerTime()
{

	if(buzzerTime>0)
	{
		buzzerTime--;
		TM_PWM_SetChannelPercent(&PWMTimerData, BUZ_PWM_CHANNEL, 75);
	}
	else
	{
		TM_PWM_SetChannelPercent(&PWMTimerData, BUZ_PWM_CHANNEL, 0);
	}
}

void BUTTON1_EventHandler(TM_BUTTON_PressType_t type) 
{
    if (type == TM_BUTTON_PressType_OnPressed)
    {
    	fUseController = ON;
    		fAlgorithm = ON;
    } 
    else if (type == TM_BUTTON_PressType_Normal) 
    {
        //printf("Normal press detected on button 1\n");
    } 
    else printf("Long press detected on button 1\n");
   
}


void BatteryLevel(void)
{
	double batLevel = 0;
	batLevel = (double)TM_ADC_Read(BAT) * 8.33/3325;
	if(batLevel < CRIT_BAT_LVL)
		DisplayData("BAT!");
}
