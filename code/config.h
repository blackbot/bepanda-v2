#ifndef CONFIG_H
#define CONFIG_H

#include "enums.h"

#define MAZE_SIZE 16

#define INIT_STARTX 1
#define INIT_STARTY 1
#define INIT_ENDX 7
#define INIT_ENDY 7

#define TURN_WEIGHT 11
#define BACK_WEIGHT 6
#define STRAIGHT_WEIGHT 1

#define HEAP_MAX_SIZE 4000
#define WALLCHECKED 3
#define WALL 2
#define CHECKED 1

#define WALL_THRESHOLD 100

#define ST first
#define ND second
#define INF 32766

int8_t fButtons;
int8_t fBatteryLevel;
int8_t fMeasureDistance;
int8_t fBuzzer;

int8_t fDisplay;
DisplayTypeNum fDisplay_TYPE;
char *fDisplay_s;
int8_t fDisplay_length;

int8_t fAlgorithm;


extern FeedbackSourceNum feedbackSource;
extern RunFlagNum runFlag;



//      Defines
/*-----------------------------*/
#define ON 			1
#define OFF 		0

extern const int32_t FIELD_DISTANCE;
extern const double CRIT_BAT_LVL;
extern const int16_t WALL_AVAILABLE;

extern int32_t distanceLeftCutOff;

//		IR Distances
/*-----------------------------*/
extern int32_t LSWall_1[2];
extern int32_t LSWall_2[2];
extern int32_t RSWall_1[2];
extern int32_t RSWall_2[2];

extern int32_t LDWall_1[2];
extern int32_t LDWall_2[2];
extern int32_t RDWall_1[2];
extern int32_t RDWall_2[2];

extern int32_t centerWall_1;
extern int32_t centerWall_2;
extern int32_t centerVisionDistance;
extern int32_t centerStickingDistance;

extern int32_t diagonalVisionDistance;
extern int32_t diagonalStickingDistance;


//      Other variables
/*-----------------------------*/

extern int8_t fUseController;
extern int32_t fieldDistance;
extern int32_t moveSpeed;
extern int32_t turnSpeed;
extern int32_t returnSpeed;
extern int32_t stopSpeed;
extern int32_t maxSpeed;

void Config(void);

#endif //CONFIG_H
