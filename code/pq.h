#ifndef PQ_H
#define PQ_H

#include "main.h"

typedef struct
{
    int8_t x, y;
    int16_t cost;
} Vertex;
Vertex Vertex_(int8_t x, int8_t y, int16_t cost);


typedef struct
{
    Vertex data[HEAP_MAX_SIZE];
    int size;
} Heap;


void Push(Heap *h, Vertex v);
Vertex Pop(Heap *h);

#endif // PQ_H
