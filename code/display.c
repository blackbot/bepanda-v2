#include "main.h"

void Display_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	SPI_InitTypeDef SPI_InitStruct;
	// enable the SPI peripheral clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	// Connect SPI pins to AF5
	GPIO_PinAFConfig(DISPLAY_MOSI_PORT, GPIO_PinSource3, GPIO_AF_SPI2);
	GPIO_PinAFConfig(DISPLAY_SCK_PORT, GPIO_PinSource10, GPIO_AF_SPI2);

	
	//MOSI C3
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin   = DISPLAY_MOSI_PIN;
	GPIO_Init(DISPLAY_MOSI_PORT, &GPIO_InitStructure);

	//SCK B10
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin   = DISPLAY_SCK_PIN;
	GPIO_Init(DISPLAY_SCK_PORT, &GPIO_InitStructure);
	
	//RS C2
	GPIO_InitStructure.GPIO_Pin = DISPLAY_RS_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(DISPLAY_RS_PORT, &GPIO_InitStructure);

	//CE B9
	GPIO_InitStructure.GPIO_Pin = DISPLAY_CE_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(DISPLAY_CE_PORT, &GPIO_InitStructure);

	//SPI    APB1=AHB1(168MHz)/4=42MHz
	// now we can set up the SPI peripheral
	// Assume the target is write only and we look after the chip select ourselves
	// SPI clock rate will be system frequency/4/prescaler
	// so here we will go for 42MHz/2 = 21MHz

	SPI_I2S_DeInit(DISPLAY_SPI);
	SPI_StructInit(&SPI_InitStruct);
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_Direction = SPI_Direction_1Line_Tx;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;

	SPI_Init(DISPLAY_SPI, &SPI_InitStruct);
	SPI_SSOutputCmd(DISPLAY_SPI, ENABLE);

	SPI_Cmd(DISPLAY_SPI, ENABLE);
	SPI_NSSInternalSoftwareConfig(DISPLAY_SPI, ENABLE);

	DISPLAY_DESELECT();
	DisplayCmd(0x77);
	DisplayClear();
}

void DisplayCmd(char c)
{
	DISPLAY_CMD();   //select control register
	DISPLAY_SELECT();	//enable data writing
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(SPI2, c);
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET);
	

	DISPLAY_DESELECT();   //latch on
	DISPLAY_DATA();	//switch back to data register
}

void DisplayData(const char *s)
{
	char const *ptr; //pointer for starting address of character s

	DISPLAY_DATA();   //select control register
	DISPLAY_SELECT();	//enable data writing

	for(int i=0; i<4; i++)
	{
		ptr = &fontTable[s[i]*5];
		for(int j=0; j<5; j++)
		{
			while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
			SPI_I2S_SendData(SPI2, *ptr);
			ptr++;
		}
	}
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
	while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET);
	DISPLAY_DESELECT();   //latch on
}

void DisplayClear(void)
{
	DISPLAY_DATA();   //select control register
	DISPLAY_SELECT();	//enable data writing

		for(int i=0; i<20; i++)
		{
			while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
			SPI_I2S_SendData(SPI2, 0x00);
		}
			while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
			while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_BSY) == SET);
	DISPLAY_DESELECT();   //latch on
}

void DisplayErr(int err)
{
	char str[5];
	snprintf(str, 5, "E%03d", err);

	DisplayData(str);
}

void DisplayInt(int i)
{
	char str[5];
	snprintf(str, 5, "%04d", i);

	DisplayData(str);
}

void DisplayDataScroll(char *str, int length)
{
	int i;

	for(i = 0; i < 3; i++) {
		if(str[i] == '\0') {
			DisplayData(str);
			return;
		}
	}

	i = 0;
	while(str[i+3] != '\0') {
		DisplayData(&str[i]);
		//CoTickDelay(1000);
		i++;
	}
}

void Display(DisplayTypeNum Type, char *s, int length)
{
	fDisplay = ON;
	fDisplay_TYPE = Type;
	fDisplay_s = s;
	fDisplay_length = length;
	return;
}

