# include <stdio.h>
# include <stdlib.h>
#include "pq.h"


Vertex Vertex_(int8_t x, int8_t y, int16_t cost)
{
    Vertex temp = { x, y, cost };
    return temp;
}

void Push(Heap *h, Vertex v)
{
	int at = h->size++;
	h->data[at] = v;
	while (at > 0 && h->data[at].cost < h->data[at/2].cost) {
		Vertex tmp = h->data[at];
		h->data[at] = h->data[at/2];
		h->data[at/2] = tmp;
		at /= 2;
	}
}

Vertex Pop(Heap *h)
{
	Vertex ret = h->data[0];
	h->data[0] = h->data[--h->size];

	int at = 0;
	while (1) {
		int son = 2*at + 1;
		if (son >= h->size) break;
		if (son+1 < h->size && h->data[son].cost > h->data[son+1].cost) ++son;
		if (h->data[at].cost > h->data[son].cost) {
			Vertex tmp = h->data[at];
			h->data[at] = h->data[son];
			h->data[son] = tmp;
			at = son;
		} else {
			break;
		}
	}
	return ret;
}
