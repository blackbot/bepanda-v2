#include "main.h"

///TODO: tymczasowa deklaracja tutaj wallAppearance[7][7]
int16_t wallAppearance[7][7];

SetupPack setup[10][10];

double curSpeedX;
double curSpeedW;

double pidInputX;
double pidInputW;
double posErrorX;
double posErrorW;
double oldPosErrorX;
double  oldPosErrorW;
int16_t posPwmX;
int16_t posPwmW;

double targetSpeedX;
double targetSpeedW;
double kpX, kdX;
double kpW, kdW;//used in straight
double accX;//6m/s/s
double decX;
double accW; //deg/s/s
double decW;

double T1;
double T2;
double T3;

//speeds
int16_t leftEncoder;
int16_t rightEncoder;
int16_t leftEncoderOld;
int16_t rightEncoderOld;
int16_t leftEncoderChange;
int16_t rightEncoderChange;
//int16_t encoderChange;
int32_t leftEncoderCount;
int32_t rightEncoderCount;
//int32_t encoderCount;
int32_t distanceLeft;
int32_t distanceLeftCutOff;
int8_t centerWallAlign;

int32_t encoderFeedbackX;
int32_t encoderFeedbackW;

int32_t distancePos;
int32_t tempPos;

double angleChange;
double angleOld;

inline double SpeedToTicks(int16_t x)
{
	return x*0.2151*2;
}

inline double DegToTicks(int16_t x)
{
	return x*0.249;
}

inline int32_t TicksToSpeed(int32_t x)
{
	return x*0.00464/2;
}

void ResetAll()
{
	fUseController = OFF;
	Motors(0,0);

	curSpeedX = 0;
	curSpeedW = 0;
	targetSpeedX = 0;
	targetSpeedW = 0;
	pidInputX = 0;
	pidInputW = 0;

	posErrorX = 0;
	posErrorW = 0;
	oldPosErrorX = 0;
	oldPosErrorW = 0;
	posPwmX = 0;
	posPwmW = 0;

	leftEncoder = 0;
	rightEncoder = 0;
	leftEncoderOld = 0;
	rightEncoderOld = 0;
	leftEncoderChange = 0;
	rightEncoderChange = 0;
	leftEncoderCount = 0;
	rightEncoderCount = 0;
	distanceLeft = 0;
	encoderFeedbackX = 0;
	encoderFeedbackW = 0;

	TIM2->CNT = 0;
	TIM4->CNT = 0;
}
double NeedToDecelerate(int32_t dist, double curSpd, double endSpd)
{
	curSpd = abs(curSpd);
	endSpd = abs(endSpd);
	if(dist <= 0)
		dist = 1;

	//printf("%d %d %d: %d\n", dist, curSpd, endSpd, abs(TicksToSpeed((curSpd*curSpd - endSpd*endSpd)*100/dist/4/2)));
	double result = (curSpd*curSpd - endSpd*endSpd)/(2*dist);

	return (result > 0)? result : -result;

	/*
	return (abs(TicksToSpeed((curSpd*curSpd - endSpd*endSpd)*100(double)dist/4/2))); //dist_counts_to_mm(dist)/2);
		//calculate deceleration rate needed with input distance, input current speed and input targetspeed to determind if the deceleration is needed
	//use equation 2*a*S = Vt^2 - V0^2  ==>  a = (Vt^2-V0^2)/2/S
	//because the speed is the sum of left and right wheels(which means it's doubled), that's why there is a "/4" in equation since the square of 2 is 4
*/
}

void EncodersRead(void)
{
	leftEncoderOld = leftEncoder;
	leftEncoder = TIM_GetCounter (ENCL_TIMER) ;
	rightEncoderOld = rightEncoder;
	rightEncoder = -TIM_GetCounter (ENCR_TIMER) ;
	leftEncoderChange = leftEncoder - leftEncoderOld;
	rightEncoderChange = rightEncoder - rightEncoderOld;

	leftEncoderCount += leftEncoderChange;
	rightEncoderCount += rightEncoderChange;

	// update distanceLeft
	distanceLeft -= (leftEncoderChange + rightEncoderChange);
	distancePos += (leftEncoderChange + rightEncoderChange);
}

void GyroRead(void)
{
	double angle = GyroGetAngle();
	angleChange = angle - angleOld;
	angleOld = angle;
}

int16_t IRRead(void)
{
	///TODO: okreslanie bledu z diod D (�eby widzie� do przodu troch�), chyba ze widzimy scianke z przodu
	int16_t LC = distanceLC, RC = distanceRC;
	if (LC < centerWall_1 && RC < centerWall_1) //obracanie na podstawie przednich sensorow
		return (RC - LC);

	int16_t RD = distanceRD, LD = distanceLD;
	if (RD > diagonalVisionDistance) RD = diagonalStickingDistance;
	else SetLed(LED_RD, ON);

	if (LD > diagonalVisionDistance) LD = diagonalStickingDistance;
	else SetLed(LED_LD, ON);

	//TODO: proporcjonalne do predkosci translacji
	return (RD-LD);
}

void UpdateCurrentSpeed()
{
	double nextActionQSpeed = 0;
	if (actionQ[(qR+1) & (ACTIONQ_SIZE - 1)].status == TODO)
		nextActionQSpeed = SpeedToTicks(setup[runFlag][actionQ[(qR+1) & (ACTIONQ_SIZE - 1)].type].speedX);

	double x = NeedToDecelerate(distanceLeft, curSpeedX, nextActionQSpeed);
	if( x >= SpeedToTicks(decX)/1000)
	{
		targetSpeedX = nextActionQSpeed;
		SetLed(LED_LC,1);
	}

	if(curSpeedX < targetSpeedX)
	{
		curSpeedX += SpeedToTicks(accX)/1000;
		if(curSpeedX > targetSpeedX)
			curSpeedX = targetSpeedX;
	}
	else if(curSpeedX > targetSpeedX)
	{
		curSpeedX -= SpeedToTicks(decX)/1000;
		if(curSpeedX < targetSpeedX)
			curSpeedX = targetSpeedX;
	}

	//**********************************************************************
	if(curSpeedW < targetSpeedW)
	{
		curSpeedW += DegToTicks(accW)/1000;
		if(curSpeedW > targetSpeedW)
			curSpeedW = targetSpeedW;
	}
	else if(curSpeedW > targetSpeedW)
	{
		curSpeedW -= DegToTicks(decW)/1000;
		if(curSpeedW < targetSpeedW)
			curSpeedW = targetSpeedW;
	}
}

void GetWallsCenter(void)
{
	if (distanceLC < centerWall_1 && distanceRC < centerWall_1)
		wallAppearance[4][3]++;
	else if (distanceLC < centerWall_2 && distanceRC < centerWall_2)
		wallAppearance[2][3]++;
}

void GetWallsSide(void)
{
	if (distanceLS <= LSWall_1[1]) //jest pierwsza scianka na lewo
	{
		if (distanceLS >= LSWall_1[0])
			wallAppearance[5][2]++;
	}
	else if (distanceLS <= LSWall_2[1]) //jest druga scianka na lewo
	{
		if (distanceLS >= LSWall_2[0])
			wallAppearance[5][0]++;
	}

	if (distanceRS <= RSWall_1[1]) //jest pierwsza scianka na prawo
	{
		if (distanceRS >= RSWall_1[0])
			wallAppearance[5][4]++;
	}
	else if (distanceRS <= RSWall_2[1]) //jest druga scianka na prawo
	{
		if (distanceRS >= RSWall_2[0])
			wallAppearance[5][6]++;
	}
}

void GetWallsDiagonal(void)
{
	//TODO: wykrywanie 3ciej scianki

	if (distanceLD <= LDWall_1[1])
	{
		if (distanceLD >= LDWall_1[0])
			wallAppearance[3][2]++;
	}
	else if (distanceLD <= LDWall_2[1])
	{
		if (distanceLD >= LDWall_2[0])
			wallAppearance[2][1]++;
	}

	if (distanceRD <= RDWall_1[1])
	{
		if (distanceRD >= RDWall_1[0])
			wallAppearance[3][4]++;
	}
	else if (distanceRD <= RDWall_2[1])
	{
		if (distanceRD >= RDWall_2[0])
			wallAppearance[2][5]++;
	}
}


void CalculateParams()
{
	ActionQProcess();

	//TODO: proste sprawdzenie gyro:
//	if ((actionQ[qR].type == TurnPivot) && (actionQ[qR].status == PROCESSING) && abs(GyroGetAngle()) >= abs(actionQ[qR].arg)){
//		targetSpeedW = 0;
//		SetLed(LED_LS, ON);
//	}
	//nic mnie kolejka nie obchodzi skoro nie ma niczego do roboty
	if (actionQ[qR].status != PROCESSING) return;

	if(actionQ[qR].type == Pivot ||
			actionQ[qR].type == Pivot180 ||
			actionQ[qR].type == TurnSmooth ||
			actionQ[qR].type == Calibrate)
	{
		if (TM_DELAY_Time() - actionQ[qR].timeStamp < T1){ //T1
			//targetSpeedW jest ok
		}
		else if (TM_DELAY_Time() - actionQ[qR].timeStamp < T1 + T2){ //T2
			//targetSpeedW jest ok
		}
		else if (TM_DELAY_Time() - actionQ[qR].timeStamp < T1 + T2 + T3) //T3
		{
			targetSpeedW = 0;
			///TODO: tutaj sprawdzanie scianke na podstawie diod side, center
			// + - - - +
			// |     S
			// | * C * <- uM
			// |     S
			// + - - - +
			GetWallsCenter();
			GetWallsSide();
		}
		else //gdy ju� koniec czasu
		{
			ActionQActionDone(qR);
		}
	}
	else if (actionQ[qR].type == Move)
	{
		//WARUNEK KONCZACY RUCH tak mniej wiecej 2mm przed koncem
		if (distanceLeft <= distanceLeftCutOff)
		{
			distanceLeft = 0;
			ActionQActionDone(qR);
		}

		int16_t distanceC = (distanceLC + distanceRC) / 2;
		if (distanceC <= centerVisionDistance && centerWallAlign == OFF)
		{
			centerWallAlign = ON;
			distanceLeft = SpeedToTicks( distanceC - centerStickingDistance) * 1000;
			SetLed(LED_RC, ON);
			printf("  FRONT ALIGN\r\n");
		}
		//wypelnianie pomocniczej mapy scianek dla aktualnego skanowania
		//   0 1 2 3 4 5 6
		// 0 + - + - + - +
		// 1 |   |   |   |
		// 2 + - + - + - +
		// 3 |   |   |   |
		// 4 + - + - + - +
		// 5 |   | Y |   |
		// 6 + - + X + - +

		//diagonalWall_2 = (2*(180 + TicksToSpeed(distanceLeft/2)))/3 - 40;
		//printf("<%d>", diagonalWall_2);

		//je�eli jestem w ruchu typu Y: mam wspolrzedne X(5,3)
		if ((posX & 1) && (posY & 1))
		{
			//printf("skanuje srodek / boki\r\n");
			GetWallsCenter();
			GetWallsDiagonal();
			//if (distancePos > FIELD_DISTANCE / 2) //skanowanie tylko w 2giej polowie pola
			//	GetWallsDiagonal();
		}
		//jezeli jestem w ruchu typu X: mam wspolrzedne Y(6,3)
		else if ((posX & 1) || (posY & 1))
		{
			//printf("skanuje srodek / przekatne\r\n");
			GetWallsCenter();
			GetWallsSide();
			//if (distancePos < FIELD_DISTANCE / 2) //skanowanie tylko w 1szej polowie pola

		}
	}
}

void CalculateMotorPWM()
{
	double gyroFeedback;
	double rotationalFeedback;
	int16_t sensorFeedback = 0;
	if (actionQ[qR].type == Move)
		sensorFeedback = IRRead();

	encoderFeedbackX = rightEncoderChange + leftEncoderChange;
	encoderFeedbackW = rightEncoderChange - leftEncoderChange;

	gyroFeedback = angleChange;

	switch(feedbackSource)
	{
		case Encoder: rotationalFeedback = encoderFeedbackW; break;
		case EncSens: rotationalFeedback = 0.8 * encoderFeedbackW + 0.2 * sensorFeedback; break;
		case EncGyro: rotationalFeedback = (encoderFeedbackW + gyroFeedback)/2; break;
		case Gyro:    rotationalFeedback = gyroFeedback; break;
	}

	posErrorX += curSpeedX - encoderFeedbackX;
	posErrorW += curSpeedW - rotationalFeedback;

	posPwmX = kpX * posErrorX + kdX * (posErrorX - oldPosErrorX);
	posPwmW = kpW * posErrorW + kdW * (posErrorW - oldPosErrorW);

	oldPosErrorX = posErrorX;
	oldPosErrorW = posErrorW;

	Motors(posPwmX - posPwmW, posPwmX + posPwmW);
}

void ControlLoop()
{
	EncodersRead();
	//GyroRead();
	IRRead();
	CalculateParams();
	UpdateCurrentSpeed();
	CalculateMotorPWM();
}
