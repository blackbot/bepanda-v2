#ifndef ACTIONQ_H
#define ACITONQ_H

#include "enums.h"

#define ACTIONQ_SIZE 32 //KONIECZNIE POTEGA 2

typedef struct{
	ActionStatusNum status;
	ActionTypeNum type;
	/* meaning of [double arg]:
		Move: 		distanceLeft
		Pivot: 		turn direction
		Pivot180: 	turn direction
		TurnSmooth: turn direction
		Calibrate:  dummy arg 		*/
	double arg;
	uint32_t timeStamp;
} actionPack;

extern actionPack actionQ[ACTIONQ_SIZE];
extern uint16_t qR;
extern uint16_t qW;
extern uint16_t actionQSize;

void ActionQInsert (ActionTypeNum type, double arg);
void ActionQProcess (void);
void ActionQActionDone (uint16_t i);
void swap(int16_t *a, int16_t *b);
#endif //ACTIONQ_H
