#ifndef IRLEDS_H
#define IRLEDS_H


extern double distanceRC;
extern double distanceRS;
extern double distanceRD;
extern double distanceLD;
extern double distanceLS;
extern double distanceLC;

void IR_Init(void);
void IR_process(ADC_TypeDef* ADCx, uint8_t channel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, int16_t gainMode, double *result);
void MeasureDistance(void);
#endif
